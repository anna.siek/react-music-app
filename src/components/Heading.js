import './Heading.css';

export function Heading({ title }) {
  return <h2 className="Heading center">{title}</h2>;
}
