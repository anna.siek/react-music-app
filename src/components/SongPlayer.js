import { useRef } from 'react';
import { Heading } from './Heading';
import './SongPlayer.css';

export function SongPlayer({ showControls = false, song }) {
  const audioRef = useRef();
  const { audioUrl, coverUrl } = song;
  return (
    <>
      <section className="SongPlayer center">
        <Heading title="Music Player" />
        <div>
          <img className="SongPlayer__img" width="300" height="300" src={coverUrl} />
        </div>
        <audio className="SongPlayer__audio" ref={audioRef} key={audioUrl} controls={showControls}>
          <source src={audioUrl} />
        </audio>
        <div>
          <button className="button" onClick={() => audioRef.current.play()}>Play</button>
          <button className="button" onClick={() => audioRef.current.pause()}>Pause</button>
        </div>
      </section>
    </>
  );
}
